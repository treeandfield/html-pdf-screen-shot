<?php
	
//--- this version of symphony requires PHP 7 ---//
require __DIR__ . '/vendor/autoload.php';

use Knp\Snappy\Pdf;

//mkdir("/Users/indigoshade/Sites/kitchensink/examples/html-pdf-screen/pdf", 0777);

$snappy = new Pdf('/usr/local/bin/wkhtmltopdf');

$html = file_get_contents('https://rickytruth.com/');
$destination = '/Users/indigoshade/Sites/kitchensink/examples/html-pdf-screen/pdf/';
$file = 'resume'.rand().'.pdf';


header('Content-Type: application/pdf');
header('Content-Disposition: attachment; filename="'.$file.'"');

print $snappy->getOutput($html);

//$snappy->generateFromHtml($html, $destination.$file);

?>
