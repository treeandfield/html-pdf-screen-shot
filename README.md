# Project Title
Proof of concept code example for integration into the larger html-pdf-screen project.

## Getting Started
Fork & Dive in. Vanilla Javascript no build system required.

## Built With
* [Coda](https://panic.com/coda/)

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

Version 0.1 Last updated 12/03/2018

## Authors

* **Rick Truhls** - *Initial work* - [http://treeandfield.com/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used.